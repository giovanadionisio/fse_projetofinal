#include "nvs_func.h"

#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "nvs_flash.h"

extern int v_saida;
extern int estado_botao;

extern void desliga_conexao();

int le_conexao_nvs(char *comodo, char *entrada, char *saida){
    ESP_ERROR_CHECK(nvs_flash_init_partition("naovolatil"));

	unsigned int tam;
    nvs_handle particao_handle;

    esp_err_t res_nvs = nvs_open_from_partition("naovolatil", "armazenamento", NVS_READWRITE, &particao_handle);
    
    if(res_nvs == ESP_ERR_NVS_NOT_FOUND){
        ESP_LOGE("NVS", "Namespace: armazenamento não encontrado busca");
    } else {
        esp_err_t res = nvs_get_str(particao_handle, "comodo", comodo, &tam);

        switch (res){
        case ESP_OK:
			nvs_get_str(particao_handle, "entrada", entrada, &tam);
			nvs_get_str(particao_handle, "saida", saida, &tam);

			nvs_close(particao_handle);

            return 0;
			break;
        default:
			nvs_close(particao_handle);
            return 1;
            break;
        }
    }
	return -1;
}

void grava_conexao_NVS(char *comodo, char *entrada, char *saida){
	ESP_ERROR_CHECK(nvs_flash_init_partition("naovolatil"));

	nvs_handle particao_handle;
    
    esp_err_t res_nvs = nvs_open_from_partition("naovolatil", "armazenamento", NVS_READWRITE, &particao_handle);
    
    if(res_nvs == ESP_ERR_NVS_NOT_FOUND){
        ESP_LOGE("NVS", "Namespace: armazenamento, não encontrado");
    }
	
	esp_err_t res = nvs_set_str(particao_handle, "comodo", comodo);
    if(res != ESP_OK){
        ESP_LOGE("NVS", "Não foi possível escrever no NVS (%s)", esp_err_to_name(res));
    }
	
	res = nvs_set_str(particao_handle, "entrada", entrada);
    if(res != ESP_OK){
        ESP_LOGE("NVS", "Não foi possível escrever no NVS (%s)", esp_err_to_name(res));
    }
	
	res = nvs_set_str(particao_handle, "saida", saida);
    if(res != ESP_OK){
        ESP_LOGE("NVS", "Não foi possível escrever no NVS (%s)", esp_err_to_name(res));
    }
    
	nvs_commit(particao_handle);
    nvs_close(particao_handle);
}

void reinicia_esp(){
    desliga_conexao();
    nvs_flash_erase_partition("naovolatil");
    esp_restart();
}

void le_botao_nvs(){
    ESP_ERROR_CHECK(nvs_flash_init_partition("naovolatil"));

    nvs_handle particao_handle;

    esp_err_t res_nvs = nvs_open_from_partition("naovolatil", "armazenamento", NVS_READWRITE, &particao_handle);

    if(res_nvs == ESP_ERR_NVS_NOT_FOUND){
        ESP_LOGE("NVS", "Namespace: armazenamento não encontrado busca");
    } else {
        esp_err_t res = nvs_get_i32(particao_handle, "estado_botao", &estado_botao);
        if(res != ESP_OK){
            ESP_LOGE("NVS", "Não foi possível escrever no NVS (%s)", esp_err_to_name(res));
        }
    }
}

void le_led_nvs(){
    ESP_ERROR_CHECK(nvs_flash_init_partition("naovolatil"));

    nvs_handle particao_handle;

    esp_err_t res_nvs = nvs_open_from_partition("naovolatil", "armazenamento", NVS_READWRITE, &particao_handle);

    if(res_nvs == ESP_ERR_NVS_NOT_FOUND){
        ESP_LOGE("NVS", "Namespace: armazenamento não encontrado busca");
    } else {
        nvs_get_i32(particao_handle, "estado_led", &v_saida);
    }
}

void grava_botao_NVS(){
    ESP_ERROR_CHECK(nvs_flash_init_partition("naovolatil"));

	nvs_handle particao_handle;
    
    esp_err_t res_nvs = nvs_open_from_partition("naovolatil", "armazenamento", NVS_READWRITE, &particao_handle);
    
    if(res_nvs == ESP_ERR_NVS_NOT_FOUND){
        ESP_LOGE("NVS", "Namespace: armazenamento, não encontrado");
    }
	
	esp_err_t res = nvs_set_i32(particao_handle, "estado_botao", estado_botao);
    if(res != ESP_OK){
        ESP_LOGE("NVS", "Não foi possível escrever no NVS (%s)", esp_err_to_name(res));
    }
	
	nvs_commit(particao_handle);
    nvs_close(particao_handle);
}

void grava_led_NVS(){
    ESP_ERROR_CHECK(nvs_flash_init_partition("naovolatil"));

	nvs_handle particao_handle;
    
    esp_err_t res_nvs = nvs_open_from_partition("naovolatil", "armazenamento", NVS_READWRITE, &particao_handle);
    
    if(res_nvs == ESP_ERR_NVS_NOT_FOUND){
        ESP_LOGE("NVS", "Namespace: armazenamento, não encontrado");
    }
	
	esp_err_t res = nvs_set_i32(particao_handle, "estado_led", v_saida);
    if(res != ESP_OK){
        ESP_LOGE("NVS", "Não foi possível escrever no NVS (%s)", esp_err_to_name(res));
    }
	
	nvs_commit(particao_handle);
    nvs_close(particao_handle);
}