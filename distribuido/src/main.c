#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "nvs_flash.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_system.h"
#include "driver/rtc_io.h"
#include "driver/ledc.h"

#include "esp32/rom/uart.h"
#include "esp_sleep.h"

#include "wifi.h"
#include "dht11.h"
#include "gpio.h"
#include "mqtt.h"
#include "parse.h"
#include "nvs_func.h"

#define BOTAO 0

char topico_config[50];
char topico_estado[50];
char idDispositivo[19];

char operacao[8];
char comodo[100];
char entrada[100];
char saida[100];

int v_saida;
int estado_botao;

xSemaphoreHandle conexaoWifiSemaphore;
xSemaphoreHandle conexaoWifi2Semaphore;
xSemaphoreHandle conexaoMQTTSemaphore;
xSemaphoreHandle mensagemMQTTSemaphore;
xSemaphoreHandle lowpowermodeSemaphore;

void calculaId(){
	uint8_t mac[6];
	esp_efuse_mac_get_default(mac);
	sprintf(idDispositivo ,"%02x:%02x:%02x:%02x:%02x:%02x",
		mac[0] & 0xff, mac[1] & 0xff, mac[2] & 0xff,
		mac[3] & 0xff, mac[4] & 0xff, mac[5] & 0xff);
}

void envia_solicitacao(){
	mqtt_envia_mensagem(topico_config, solicita_configuracao(operacao, idDispositivo));
}

void envia_mensagem_estado(){
	mqtt_envia_mensagem(topico_estado, mensagem_estado(comodo, estado_botao));
}

void controla_temp(){
    DHT11_init();

    xTaskCreate(&calcula_media, "Calcular Temp", 2098, NULL, 1, NULL);
    xTaskCreate(&envia_dados_temperatura, "Enviar Temp", 2098, NULL, 1, NULL);
}

void inicia_NVS(){
    esp_err_t ret = nvs_flash_init();
    if(ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND){
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
}

void realizando_conexao(void * params){
	while(true){
		if(xSemaphoreTake(conexaoWifi2Semaphore, portMAX_DELAY) && xSemaphoreTake(conexaoWifiSemaphore, portMAX_DELAY)){
			calculaId();
			strcpy(topico_config, "fse2021/180017659/dispositivos/");
			strcat(topico_config, idDispositivo);
			mqtt_start();
			if(xSemaphoreTake(conexaoMQTTSemaphore, portMAX_DELAY)){
				if(le_conexao_nvs(comodo, entrada, saida) == 1){
					envia_solicitacao();
					vTaskDelay(1000 / portTICK_PERIOD_MS);
					mqtt_client_subscribe(topico_config);
					if(xSemaphoreTake(mensagemMQTTSemaphore, portMAX_DELAY)){
						printf("====== Controlando ======\nComodo:%s\nEntrada:%s\nSaida:%s\n", comodo, entrada, saida);
						
						strcpy(topico_estado, "fse2021/180017659/");
						strcat(topico_estado, comodo);
						strcat(topico_estado, "/estado");

						grava_conexao_NVS(comodo, entrada, saida);
						
						estado_botao = 0;
						#ifdef CONFIG_MODO_ENERGIA
							v_saida = 0;

							inicia_led();
							inicia_sensor();

							grava_botao_NVS();
							grava_led_NVS();

							controla_temp();
						#else
							botao_modo_low_power();
							grava_botao_NVS();
						#endif
					}
				} else {
					strcpy(topico_estado, "fse2021/180017659/");
					strcat(topico_estado, comodo);
					strcat(topico_estado, "/estado");

					mqtt_client_subscribe(topico_config);
					#ifdef CONFIG_MODO_ENERGIA
						printf("====== Controlando ======\nComodo:%s\nEntrada:%s\nSaida:%s\n", comodo, entrada, saida);
						le_botao_nvs();
						le_led_nvs();
						printf("======= Valores =======\n%s: %d\n%s: %d%%", entrada, estado_botao, saida, v_saida);
	
						inicia_led();
						inicia_sensor();

						controla_temp();
					#else
						le_botao_nvs();
						printf("======= Valores =======\n%s: %d\n", entrada, estado_botao);

						botao_modo_low_power();
					#endif
				}
			}
			xSemaphoreGive(lowpowermodeSemaphore);
			xSemaphoreGive(conexaoWifi2Semaphore);
		}
  	}
}

void inicia_conexao(){
    conexaoWifiSemaphore = xSemaphoreCreateBinary();
	conexaoWifi2Semaphore = xSemaphoreCreateBinary();
    conexaoMQTTSemaphore = xSemaphoreCreateBinary();
	lowpowermodeSemaphore = xSemaphoreCreateBinary();
	mensagemMQTTSemaphore = xSemaphoreCreateBinary();

	xSemaphoreGive(conexaoWifi2Semaphore);
    wifi_start();
    xTaskCreate(&realizando_conexao, "Conexão ao MQTT", 4096, NULL, 1, NULL);
}

void desliga_conexao(){
	mqtt_client_stop();
	esp_wifi_stop();
	esp_wifi_deinit();
}

void liga_conexao(){
	wifi_reconnect();
	if(esp_wifi_start() == ESP_OK){
		if(xSemaphoreTake(conexaoWifiSemaphore, portMAX_DELAY)){
			mqtt_client_start();
			if(xSemaphoreTake(conexaoMQTTSemaphore, portMAX_DELAY)){
				envia_mensagem_estado();
			}
		}
	}
}

void lowPowerMode(void *params){
	if(xSemaphoreTake(lowpowermodeSemaphore, portMAX_DELAY)){
		xSemaphoreTake(conexaoWifi2Semaphore, portMAX_DELAY);
		esp_sleep_enable_gpio_wakeup();	
		strcpy(topico_estado, "fse2021/180017659/");
		strcat(topico_estado, comodo);
		strcat(topico_estado, "/estado");
	}

	while(true){
		if (rtc_gpio_get_level(BOTAO) == 0){
			int contador_tempo = 0;
            while(rtc_gpio_get_level(BOTAO) == 0){
                vTaskDelay(50 / portTICK_PERIOD_MS);
                contador_tempo++;
                if(contador_tempo == 50){
                    inicia_led();
					liga_led(100);
                    vTaskDelay(1000 / portTICK_PERIOD_MS);
                    liga_led(0);
                    vTaskDelay(1000 / portTICK_PERIOD_MS);
                    liga_led(100);
                    vTaskDelay(1000 / portTICK_PERIOD_MS);
                    liga_led(0);
                    
                    printf("Reiniciando!\n");
                    reinicia_esp();
                    break;
                }
            }
		}

		printf("Entrando em modo Low Power\n");
		desliga_conexao();
		uart_tx_wait_idle(CONFIG_ESP_CONSOLE_UART_NUM);
		
		esp_light_sleep_start();	
		
		estado_botao = !estado_botao;
		liga_conexao();		
		grava_botao_NVS();
		
		printf("Estado botão: %d\n", estado_botao);
	}

}

void app_main(){
    inicia_NVS();

	#ifdef CONFIG_MODO_ENERGIA
		strcpy(operacao, "energia");
		inicia_conexao();
    #else
		strcpy(operacao, "bateria");
		inicia_conexao();
		
		xTaskCreate(&lowPowerMode, "Modo low Power", 4096, NULL, 1, NULL);
    #endif	
}