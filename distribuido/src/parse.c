#include "parse.h"

#include <stdlib.h>
#include <stdio.h>
#include <cJSON.h>
#include <string.h>
#include <math.h>

#include "gpio.h"

extern char comodo[100];
extern char entrada[100];
extern char saida[100];
extern int v_saida;


void mensagem_dispositivo(char mensagem[]){
    cJSON *root = cJSON_Parse(mensagem);
    cJSON *m_comando = cJSON_GetObjectItem(root, "set");

    v_saida = m_comando->valueint;
    printf("Comando: %d\n", v_saida);
    liga_led(v_saida);
}

void mensagem_configuracao(char mensagem[]){
    cJSON *root = cJSON_Parse(mensagem);
    cJSON *m_comodo = cJSON_GetObjectItem(root, "comodo");
    cJSON *m_entrada = cJSON_GetObjectItem(root, "entrada");
    cJSON *m_saida = cJSON_GetObjectItem(root, "saida");

    stpcpy(comodo, m_comodo->valuestring);
    stpcpy(entrada, m_entrada->valuestring);
    stpcpy(saida, m_saida->valuestring);
}

char *solicita_configuracao(char operacao[], char id[]){
    cJSON *root = NULL;
    root = cJSON_CreateObject();
    cJSON_AddItemToObject(root, "operacao", cJSON_CreateString(operacao));
    cJSON_AddItemToObject(root, "id", cJSON_CreateString(id));

    return cJSON_Print(root);
}

char *mensagem_temperatura(float temperatura){
    char arr_temperatura[100];
    sprintf(arr_temperatura, "%.2f", temperatura);
    
    cJSON *root = NULL;
    root = cJSON_CreateObject();

    cJSON_AddItemToObject(root, "local", cJSON_CreateString(comodo));
    cJSON_AddItemToObject(root, "temperatura", cJSON_CreateNumber(atof(arr_temperatura)));

    return cJSON_Print(root);
}

char *mensagem_umidade(float umidade){
    char arr_umidade[100];
    sprintf(arr_umidade, "%.2f", umidade);

    cJSON *root = NULL;
    root = cJSON_CreateObject();

    cJSON_AddItemToObject(root, "local", cJSON_CreateString(comodo));
    cJSON_AddItemToObject(root, "umidade", cJSON_CreateNumber(atof(arr_umidade)));

    return cJSON_Print(root);
}

char* mensagem_estado (char local[], int estado){
    cJSON *root = NULL;

    root = cJSON_CreateObject();
    cJSON_AddItemToObject(root, "local", cJSON_CreateString(local));
    cJSON_AddItemToObject(root, "estado", cJSON_CreateNumber(estado));

    return cJSON_Print(root);
}