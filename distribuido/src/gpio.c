#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "driver/ledc.h"

#include "gpio.h"

xQueueHandle filaInterrupcao;

extern int estado_botao;
extern int v_saida;

extern void envia_mensagem_estado();
extern void grava_botao_NVS();
extern void grava_led_NVS();
extern void reinicia_esp();

#define BOTAO 0
#define LED 2

static void IRAM_ATTR gpio_isr_handler(void *args){
    int pino = (int)args;
    xQueueSendFromISR(filaInterrupcao, &pino, NULL);
}

void configura_interrupcao(){
    filaInterrupcao = xQueueCreate(10, sizeof(int));
    xTaskCreate(trataInterrupcaoBotao, "TrataBotao", 2048, NULL, 1, NULL);

    gpio_install_isr_service(0);
    gpio_isr_handler_add(BOTAO, gpio_isr_handler, (void *) BOTAO);
}

void liga_led(int intensidade){
    if(intensidade < 0) {
        ledc_set_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0, (0));
        ledc_update_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0);

        reinicia_esp();
    } else {
        ledc_set_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0, (255*intensidade)/100);
        ledc_update_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0); 

        grava_led_NVS();
    }
}

void inicia_led(){
    ledc_timer_config_t timer_config = {
        .speed_mode = LEDC_LOW_SPEED_MODE,
        .duty_resolution = LEDC_TIMER_8_BIT,
        .timer_num = LEDC_TIMER_0,
        .freq_hz = 1000,
        .clk_cfg = LEDC_AUTO_CLK
    };
    ledc_timer_config(&timer_config);

    ledc_channel_config_t channel_config = {
        .gpio_num = LED,
        .speed_mode = LEDC_LOW_SPEED_MODE,
        .channel = LEDC_CHANNEL_0,
        .timer_sel = LEDC_TIMER_0,
        .duty = 0,
        .hpoint = 0
    };
    ledc_channel_config(&channel_config);

    liga_led(v_saida);
}

void inicia_sensor(){
    gpio_pad_select_gpio(BOTAO);
    gpio_set_direction(BOTAO, GPIO_MODE_INPUT);

    gpio_pullup_en(BOTAO);
    gpio_pulldown_dis(BOTAO);

    gpio_set_intr_type(BOTAO, GPIO_INTR_NEGEDGE);
    configura_interrupcao();
}

void botao_modo_low_power(){
    gpio_pad_select_gpio(BOTAO);
    gpio_set_direction(BOTAO, GPIO_MODE_INPUT);

    gpio_wakeup_enable(BOTAO, GPIO_INTR_LOW_LEVEL);
}

void trataInterrupcaoBotao(void *params){
    int pino;
    
    while(true){
        if(xQueueReceive(filaInterrupcao, &pino, portMAX_DELAY)){
            int estado = gpio_get_level(pino);
            if(estado == 0){
                gpio_isr_handler_remove(pino);
                int contador_tempo = 0;
                while(gpio_get_level(pino) == estado){
                    vTaskDelay(50 / portTICK_PERIOD_MS);
                    contador_tempo++;
                    if(contador_tempo == 50){
                        liga_led(100);
                        vTaskDelay(1000 / portTICK_PERIOD_MS);
                        liga_led(0);
                        vTaskDelay(1000 / portTICK_PERIOD_MS);
                        liga_led(100);
                        vTaskDelay(1000 / portTICK_PERIOD_MS);
                        liga_led(0);
                        
                        printf("Reiniciando!\n");
                        reinicia_esp();
                        break;
                    }
                }
                
                estado_botao = !estado_botao;
                grava_botao_NVS();
                printf("Estado: %d\n", estado_botao);
                envia_mensagem_estado();

                // Habilitar novamente a interrupção
                vTaskDelay(50 / portTICK_PERIOD_MS);
                gpio_isr_handler_add(pino, gpio_isr_handler, (void *) pino);
            }

        }
    }  
}