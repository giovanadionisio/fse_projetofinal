#ifndef PARSE_H_
#define PARSE_H_

char* mensagem_estado (char local[], int estado);
void mensagem_configuracao(char mensagem[]);
char *solicita_configuracao(char operacao[], char id[]);
void mensagem_dispositivo(char mensagem[]);
char* mensagem_temperatura(float temperatura);
char* mensagem_umidade(float umidade);

#endif