#ifndef NVS_FUNC_H_
#define NVS_FUNC_H_

int le_conexao_nvs(char *comodo, char *entrada, char *saida);
void grava_conexao_NVS(char *comodo, char *entrada, char *saida);
void le_botao_nvs();
void le_led_nvs();
void grava_botao_NVS();
void grava_led_NVS();
void reinicia_esp();

#endif