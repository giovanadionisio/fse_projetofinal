#ifndef GPIO_H_
#define GPIO_H_

void inicia_sensor();
void inicia_led();
void configura_interrupcao();
void trataInterrupcaoBotao();
void botao_modo_low_power();
void liga_led(int intensidade);

#endif