
var mqtt;
var reconnectTimeout = 2000;
var host = "broker.hivemq.com";
var port = 8000;
var out_msg;
var alarm_on = 0;
var esps = [];
var comodos = [];
var csvContent = [];
var selectDisc;
var rooms;

function onConnect() {
    console.log("Connected");
    initCSV();
    mqtt.subscribe("fse2021/180017659/dispositivos/#");
    mqtt.subscribe("fse2021/180017659/#");
}
function MQTTconnect() {
    console.log("connecting to " + host + " " + port);
    mqtt = new Paho.MQTT.Client(host, port, "clientjs");
    var options = {
        timeout: 3,
        onSuccess: onConnect,
    };
    mqtt.onMessageArrived = onMessageArrived;
    mqtt.connect(options);
}
function onMessageArrived(msg) {
    out_msg = msg.payloadString;
    console.log('out_msg: ',out_msg);
    parseMsg(out_msg);
}
function parseMsg(arg) {
    out_msg = JSON.parse(arg);
    console.log(out_msg);
    const {
        operacao,
        id,
        local,
        temperatura,
        umidade,
        estado
    } = out_msg;
    if(id && operacao){
        var aux = 0;
        esps.forEach(esp => {
            if(esp.id == id)
                aux = 1;
        })
        if(aux == 0)
            esps.push({id, operacao});
        var selectBox = document.getElementById('esps');
        selectBox.options.add(new Option(id));
    }
    if(local){
        comodos.forEach(comodo => {
            if(comodo.comodo == local) {
                if(temperatura)
                    comodo.temperatura = temperatura;
                else if(umidade)
                    comodo.umidade = umidade;
                else if(estado == 1 || estado == 0){
                    comodo.entrada_estado = estado;
                    createCSV(comodo.comodo, comodo.entrada, comodo.entrada_estado);
                    if(comodo.entrada_estado == 1 && comodo.alarme == 'y')
                        soundAlarm(comodo.comodo, comodo.entrada);
                }
            }
        })
    }
    loopDisps();
}
function registerRoom(){
    const id_comodo = document.getElementsByName("esp-id-name")[0].value.toString();
    const topic = "fse2021/180017659/dispositivos/" + id_comodo;
    console.log('id name', id_comodo);
    var saida_aux = '';
    var check_aux = 0;
    var operacao_aux = '';
    esps.forEach(esp => {
        if(esp.id == id_comodo && esp.operacao == "energia") {
            operacao_aux = esp.operacao;
            saida_aux = document.getElementsByName("output-name")[0].value;
        } 
    })
    comodos.forEach(comodo => {
        if(comodo.comodo == document.getElementsByName("room-name")[0].value)
            check_aux = 1;
    })
    if(check_aux == 0){       
        const message = {
            comodo: document.getElementsByName("room-name")[0].value,
            entrada: document.getElementsByName("input-name")[0].value,
            saida: saida_aux
        };
        const new_room = {
            id: id_comodo.toString(),
            operacao: operacao_aux,
            comodo: document.getElementsByName("room-name")[0].value,
            entrada: document.getElementsByName("input-name")[0].value,
            alarme: document.getElementsByName("alarm")[0].value,
            saida: saida_aux
        };
        mqtt.send(topic, JSON.stringify(message));
        comodos.push(new_room);
        createCSV(document.getElementsByName("room-name")[0].value, id_comodo, 'cadastro dispositivo');
        loopDisps();
        selectDisc = document.getElementById('comodos');
        selectDisc.options.add(new Option(document.getElementsByName("room-name")[0].value));
        var room_aux = document.getElementsByName("room-name")[0].value;
        comodos.forEach(comodo => {
            if(comodo.comodo == room_aux && comodo.operacao == 'energia'){
                rooms = document.getElementById('rooms');
                rooms.options.add(new Option(comodo.comodo));
            }
        })
    }
}
function loopDisps() {
    var div = document.getElementById("disps");
    div.innerHTML = null;
    comodos.forEach(comodo => {
        var html_disps = `<div class="disp-box" id="comodos">
                                <div class="disp-box-nav">
                                    <h3 id="local" class="title" name="local">${comodo.comodo ? comodo.comodo : ''}</h3>
                                    <h3 id="id_comodo" class="title">${comodo.comodo ? comodo.id : ''}</h3>
                                </div>
                                <div class="disp-box-data">
                                    <div class="block">
                                        <p><strong>Temperatura: </strong><p>${comodo.temperatura ? comodo.temperatura : ''}</p><strong>${comodo.operacao == 'energia' ? '°C' : ': low power'}</strong></p>
                                        <p><strong>Umidade: </strong><p>${comodo.umidade ? comodo.umidade : ''}</p><strong>${comodo.operacao == 'energia' ? '%' : ': low power'}</strong></p>
                                        <p class="input-state"><strong>Entrada </strong><strong class="input">${comodo.entrada ? comodo.entrada : ''}</strong>: <p>${comodo.entrada_estado == 1 ? 'Ligado' : 'Desligado'}</p></p>
                                        <p class="output-state"><strong>Saída </strong><strong class="output" id="saida_nome">${comodo.saida ? comodo.saida + ': ' : ': low power'}</strong><p>${comodo.saida_estado && comodo.saida ? comodo.saida_estado : ''}</p></p>
                                    </div>
                                </div>
                            </div>`
        div.innerHTML += html_disps;
    })
}
function sendMessage() {
    var id;
    var index;
    comodos.forEach((comodo, i) => {
        if(comodo.comodo == document.getElementsByName("comodo_nome")[0].value){
            id = comodo.id;
            index = i;
        }
    })
    const topic = "fse2021/180017659/dispositivos/" + id;
    var saida_estado = Number(document.getElementsByName("saida")[0].value);
    const saida_nome = comodos[index].saida;
    if(saida_nome != 'LAMPADA' && saida_estado > 0){
        saida_estado = 100;
    }
    else if(saida_estado > 100) {
        saida_estado = 100;
    }
    else if(saida_estado < 0) {
        saida_estado = 0;
    }
    comodos[index].saida_estado = saida_estado;
    const message = {
        set: saida_estado
    };
    console.log('message', message);
    mqtt.send(topic, JSON.stringify(message));
    createCSV(comodos[index].comodo, saida_nome, saida_estado);
}
function disconectDisp() {
    var id = '';
    var index = '';
    var room = document.getElementsByName("esp-comodo")[0].value.toString();
    comodos.forEach((comodo, i) => {
        if(comodo.comodo == room){
            id = comodo.id;
            comodos.splice(i, 1);
            index = i;
        }
    })
    const message = {
        set: -1
    };
    const topic = "fse2021/180017659/dispositivos/" + id;
    console.log('message', message);
    mqtt.send(topic, JSON.stringify(message));
    loopDisps();
}
function turnOnAlarm() {
    if(alarm_on == 0){
        alarm_on = 1;
        document.getElementById('alarm_state').innerHTML = 'Ligado';
    }
    else{
        alarm_on = 0;
        document.getElementById('alarm_state').innerHTML = 'Desligado';
    }
}
function soundAlarm(local, entrada) {
    if(alarm_on == 1) {
        var audio = new Audio('http://www.healthfreedomusa.org/downloads/iMovie.app/Contents/Resources/iMovie%20%2708%20Sound%20Effects/Alarm.mp3');
        audio.play();
        createCSV(local, entrada, 'alarme on');
    }
}
function initCSV() {
    data = ['DATA', 'HORARIO', 'COMODO', 'DISPOSITIVO', 'ACAO'];
    data.join(';');
    csvContent = data + '\n';
}
function createCSV(local, disp, acao) {
    var today = new Date();
    var date =  today.getDate() + '-' + (today.getMonth()+1) + '-' + today.getFullYear();
    var time = today.getHours() + "h" + today.getMinutes() + "m" + today.getSeconds() + "s";
    data = [date, time, local, disp, acao];
    data.join(';');
    csvContent += data + '\n';
}
function downloadCSV() {
    var download = function(content, fileName, mimeType) {
        var a = document.createElement('a');
        mimeType = mimeType || 'application/octet-stream';
      
        if (navigator.msSaveBlob) { // IE10
          navigator.msSaveBlob(new Blob([content], {
            type: mimeType
          }), fileName);
        } else if (URL && 'download' in a) { //html5 A[download]
          a.href = URL.createObjectURL(new Blob([content], {
            type: mimeType
          }));
          a.setAttribute('download', fileName);
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
        } else {
          location.href = 'data:application/octet-stream,' + encodeURIComponent(content); // only this mime type is supported
        }
      }
    download(csvContent, 'logs.csv', 'text/csv;encoding:utf-8');
}