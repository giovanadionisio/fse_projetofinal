# FSE - Projeto Final

Enunciado: https://gitlab.com/fse_fga/projetos_2021_1/projeto-final-2021-1

Vídeo (apresentação): https://www.youtube.com/watch?v=UvTB19MeNm0
## Alunas

- Gabriela da Gama Pivetta - 180052845
- Giovana Vitor Dionisio Santana - 180017659

## Elaboração
A elaboração do projeto do servidor distribuído foi realizada com o auxílio da extensão [PlatformIO](https://platformio.org/) na versão na versão 2.4.0. 

## Execução
Clonar ou baixar os arquivos do repositório. Em seguida **é necessário rodar primeiro o servidor central e para depois executar o distribuido.**
 
### **Central**

Abrir o arquivo "**interface.html**" (que está na pasta "front") em algum navegador, basta dar dois cliques. 

Não foi utilizado nenhum framework, então não há necessidade de instalação de dependências nem de se utilizar um docker.
### **Distribuido**
Para a execução do servidor distribuído é necessária a extensão PlatformIO para o VSCode. No menu 'Platform' da extensão, selecione a opção 'Run Menuconfig' para a configuração das credenciais do Wifi (Configuração do Wifi) e do modo de operação, se Energia ou Bateria (Modo de operação).  
Após as configuraços realizadas no Menuconfig, selecione a opção 'Build' dentro do menu 'General' da extensão. Após a compilação de todos os arquivos, utilize a opção 'Monitor' para a execução do programa.  
- **Controle do Botão:** Por padrão, ao conectar-se a uma ESP, o valor do sensor será 0. A cada vez que o botão for apertado, este valor é alternado entre 1 e 0. 

- **Reinicialização Manual:** Uma ESP já conectada ao servidor central pode ser reinicializada ao presionar-se o botão 'BOOT' por cerca de 4 segundos. O LED da placa irá piscar duas vezes, sinalizando que a ESP está sendo reinicializada e que já é possível soltar o botão. 

## Observações

1. **Saídas:** Para controlar as saídas existe um campo onde se pode digitar qualquer número. Sempre que o valor for negativo ou 0, a saída será desligada. Já para ligar, existem dois casos:
    - **Lâmpada:** as lâmpadas são dimerizáveis, então ela recebe valores de 1 a 100 de acordo com a intensidade que deve acender. Se o valor for superior a 100, será enviado 100.  

    - **Outros:** demais saídas apenas ligam em uma única intensidade, então qualquer valor positivo será interpretado como 100.

2. **CSV:** Por estar sendo executado em uma plataforma WEB, o servidor central não possui acesso aos arquivos locais do computador. Por isso, para acessar o log dos acionamentos dos dispositivos, basta clicar no botão 'Download CSV Log'.

3. **Alarme:** O alarme precisa de duas condições para ser ativado:
    - Seu botão, no servidor central deve estar "Ligado"

    - Receber uma mensagem da entrada de um servidor distribuido, que tem a capacidade de acionar o alarme, como "Ligado"   

4. **Partições:** Para o correto funcionamento do servidor distribuído, o arquivo sdkconfig já presente neste repositório não deve ser deletado. Caso contrário, será necessário, no Menuconfig da ESP, atualizar as configurações de partição do projeto, utilizando a Partição Costumizada 'partitions.csv' presente no diretório do servidor distribuído. 

## Interface
![img](https://i.ibb.co/GWLgZ5t/Captura-de-tela-de-2021-11-07-22-55-24.png)